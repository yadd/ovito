.. _file_formats.input.gsd:

GSD/HOOMD file reader
---------------------

.. figure:: /images/io/gsd_reader.*
  :figwidth: 30%
  :align: right

  User interface of the GSD file reader, which appears as part of a pipeline's :ref:`file source <scene_objects.file_source>`.

Binary file format written by the `HOOMD-blue <https://glotzerlab.engin.umich.edu/hoomd-blue/>`__ molecular dynamic simulation code.
For a specification of the file format, see the `GSD (General Simulation Data) website <https://gsd.readthedocs.io>`__.

The file reader supports loading `particle shape definitions <https://gsd.readthedocs.io/en/latest/shapes.html>`__ from a GSD file
and will automatically generate corresponding polygons (2d) or meshes (3d) based on these definitions
to render :ref:`particles with user-defined shapes <howto.aspherical_particles>`.

.. _file_formats.input.gsd.python:

Python parameters
"""""""""""""""""

The file reader accepts the following optional keyword parameters in a call to the :py:func:`~ovito.io.import_file` or :py:meth:`~ovito.pipeline.FileSource.load` Python functions.

.. py:function:: import_file(location, resolution = 4)
  :noindex:

  :param resolution: Controls the resolution of shape meshes generated by the file reader if the GSD file contains `particle shape definitions <https://gsd.readthedocs.io/en/latest/shapes.html>`__
                     of type ``Polygon`` or ``ConvexPolyhedron`` with a non-zero rounding radius. The *resolution* value must be in the range 1 to 6 -- with higher values yielding smoother corners
                     but also resulting in a more detailed mesh, which will lead to longer rendering times.
  :type resolution: int
